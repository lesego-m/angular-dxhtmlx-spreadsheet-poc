import { Injectable } from '@angular/core';
import {Spreadsheet} from "../third-party/spreadsheet_trial";

import {ISpreadsheetConfig} from "../third-party/spreadsheet_trial/codebase/types/ts-spreadsheet/sources/types";

@Injectable({
  providedIn: 'root'
})
export class SpreadSheetService {

  private csvLargeData = 'assets/aggregated_activity_sample.csv'; // 3 thousand rows
  private csvData = 'assets/thousandRows.csv';
  public spreadSheet!: Spreadsheet;

  constructor() { }

  public initializeWithCsvData(element: any, options: ISpreadsheetConfig): void {
    this.spreadSheet = new Spreadsheet(element, options);
    this.spreadSheet.load(this.csvLargeData, 'csv').catch(e => console.log(e));
  }
  public initializeWithLocalData(element: any, options: ISpreadsheetConfig, data: any): void {
    this.spreadSheet = new Spreadsheet(element, options);
    this.spreadSheet.parse(data);
  }
}
