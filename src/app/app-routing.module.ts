import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {DaterangeComponent} from "./ui/daterange/daterange.component";
import {ValidationComponent} from "./ui/validation/validation.component";
import {XSpreadSheetComponent} from "./x-spread-sheet/x-spread-sheet.component";

const routes: Routes = [
  {path: '', component: ValidationComponent},
  {path: 'date-range', component: DaterangeComponent},
  {path: 'xSpreadSheet', component: XSpreadSheetComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
