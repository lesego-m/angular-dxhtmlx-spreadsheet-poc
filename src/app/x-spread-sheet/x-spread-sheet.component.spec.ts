import { ComponentFixture, TestBed } from '@angular/core/testing';

import { XSpreadSheetComponent } from './x-spread-sheet.component';

describe('XSpreadSheetComponent', () => {
  let component: XSpreadSheetComponent;
  let fixture: ComponentFixture<XSpreadSheetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ XSpreadSheetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(XSpreadSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
