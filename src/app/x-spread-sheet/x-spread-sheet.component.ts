import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-x-spread-sheet',
  templateUrl: './x-spread-sheet.component.html',
  styleUrls: ['./x-spread-sheet.component.css']
})
export class XSpreadSheetComponent implements OnInit {
  @ViewChild('x-spreadsheet-demo', {static: true}) container!: ElementRef;

  constructor() { }

  ngOnInit(): void {
  }

}
