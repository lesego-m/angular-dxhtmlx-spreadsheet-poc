import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {SpreadSheetComponent} from './spread-sheet/spread-sheet.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DaterangeComponent} from './ui/daterange/daterange.component';
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatFormFieldModule} from "@angular/material/form-field";
import {SelectComponent} from './ui/select/select.component';
import {MAT_DATE_FORMATS, MatNativeDateModule, MatOptionModule} from "@angular/material/core";
import {MatSelectModule} from "@angular/material/select";
import {AppRoutingModule} from './app-routing.module';
import { ValidationComponent } from './ui/validation/validation.component';
import {MatInputModule} from "@angular/material/input";
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MatMomentDateModule} from '@angular/material-moment-adapter';
import { XSpreadSheetComponent } from './x-spread-sheet/x-spread-sheet.component';

@NgModule({
  declarations: [
    AppComponent,
    SpreadSheetComponent,
    DaterangeComponent,
    SelectComponent,
    ValidationComponent,
    XSpreadSheetComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    AppRoutingModule,
    MatMomentDateModule,
    MatInputModule,
  ],
  bootstrap: [AppComponent],
  providers: [
    {provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: {useUtc: true}}
  ],
  entryComponents: [
    AppComponent
  ]
})
export class AppModule {
}
