import {AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {SpreadSheetService} from "../spread-sheet.service";
import {ISpreadsheetConfig} from "../../third-party/spreadsheet_trial/codebase/types/ts-spreadsheet/sources/types";

@Component({
  selector: 'app-spread-sheet',
  templateUrl: './spread-sheet.component.html',
  styleUrls: ['./spread-sheet.component.css']
})
export class SpreadSheetComponent implements OnInit, AfterViewInit {

  @ViewChild('widget', {static: true}) container!: ElementRef;
  private configSpreadSheet: ISpreadsheetConfig = {
    editLine: true,
    //colsCount: 7,
    rowsCount: 20,
    toolbarBlocks: [
      "undo", "decoration", "align", "lock", "clear",
      "rows", "columns", "help", "format", "file"
    ]
  };

  constructor(
    private spreadSheetService: SpreadSheetService,
    private cd: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    if(this.getData()) {
      this.spreadSheetService.initializeWithLocalData(this.container.nativeElement, this.configSpreadSheet, JSON.parse(this.getData() as string));
    } else {
      this.spreadSheetService.initializeWithCsvData(this.container.nativeElement, this.configSpreadSheet);
    }
    this.spreadSheetService.spreadSheet.lock("A1:G1");
  }

  ngAfterViewInit(): void {
    this.spreadSheetService.spreadSheet.events.on("afterValueChange", (cell, value) => {
      const saveData = this.spreadSheetService.spreadSheet.serialize();
      if(this.isDateRange(cell)) {
        if(!this.isValidDate(value)) {
        console.warn(`The date ${value} is invalid`);
        this.spreadSheetService.spreadSheet.setStyle(cell, { borderBottom: "1px solid #ff0000", color: "#ff0000" });
        } else {
          this.spreadSheetService.spreadSheet.setStyle(cell, {});
        }
      }

      this.setData(saveData);

      this.cd.detectChanges();
    });
  }

  private isDateRange(cell: string): boolean {
    return cell.startsWith('A', 0);
  }

  private isValidDate(dateString: string): boolean {
    const regEx = /^\d{4}-\d{2}-\d{2}$/;
    if(!dateString.match(regEx)) return false;  // Invalid format
    const d = new Date(dateString);
    const dNum = d.getTime();
    if(!dNum && dNum !== 0) return false; // NaN value, Invalid date
    return d.toISOString().slice(0,10) === dateString;
  }

  private setData(data: any): void { // TODO: 'data' needs interface
    const jsonData = JSON.stringify(data)
    localStorage.setItem('spreadsheetData', jsonData)
  }

  private getData(): string | null {
    return localStorage.getItem('spreadsheetData')
  }
}
