import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit, ViewChild
} from '@angular/core';
import {
  ISpreadsheetConfig,
  ICellInfo
} from "../../../third-party/spreadsheet_trial/codebase/types/ts-spreadsheet/sources/types";
import {SpreadSheetService} from "../../spread-sheet.service";
import {MatDatepicker} from "@angular/material/datepicker";
import * as moment from 'moment';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from "@angular/material/core";
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from "@angular/material-moment-adapter";

@Component({
  selector: 'app-daterange',
  templateUrl: './daterange.component.html',
  styleUrls: ['./daterange.component.css'],

  providers:[
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
  ]
})
export class DaterangeComponent implements OnInit, AfterViewInit {

  @ViewChild('widget', {static: true}) container!: ElementRef;

  @ViewChild('picker', {static: true}) picker!: MatDatepicker<any>;

  @ViewChild('selectedDate', {static: false}) selectedDate!: ElementRef;


  private _currentCell: string = '';
  private _saveDataSheetName: string = 'saveToDatabase';

  private configSpreadSheet: ISpreadsheetConfig = {
    editLine: true,
    toolbarBlocks: [
      "undo", "decoration", "align", "lock", "clear",
       "help", "format", "file"
    ]
  };

  constructor(
    private spreadSheetService: SpreadSheetService,
    private cd: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    if(this.getData('spreadsheetData')) {
      this.spreadSheetService.initializeWithLocalData(this.container.nativeElement, this.configSpreadSheet, JSON.parse(this.getData('spreadsheetData') as string));
    } else {
      this.spreadSheetService.initializeWithCsvData(this.container.nativeElement, this.configSpreadSheet);
    }
    this.spreadSheetService.spreadSheet.lock("A1:G1");
  }

  ngAfterViewInit(): void {
    const item = this.spreadSheetService.spreadSheet.contextMenu.data.add({
      icon: "mdi mdi-eyedropper-variant",
      value: "Segment",
      id: "segment"
    });

    this.spreadSheetService.spreadSheet.contextMenu.data.add({
      icon: "mdi mdi-eyedropper-variant",
      value: "US|IOS",
      id: "segment-US|IOS"
    }, 0, item);

    this.spreadSheetService.spreadSheet.contextMenu.events.on('click', (value, event) => {
      this.spreadSheetService.spreadSheet.setValue(this._currentCell, value);
    })

    this.spreadSheetService.spreadSheet.events.on("afterSelectionSet" , (cell,value) => {
      this._currentCell = cell;
    });

    this.spreadSheetService.spreadSheet.events.on("afterEditStart" , (cell,value) => {
      if(this.isDateRange(cell)) {
        this.picker.open();
        const sub = this.picker.closedStream.subscribe(v => {
          this.spreadSheetService.spreadSheet.setValue(cell, this.selectedDate.nativeElement.value)
          sub.unsubscribe();
        })
      }
    });

    this.spreadSheetService.spreadSheet.events.on("afterValueChange", (cell, value) => {
    //  const saveData = this.spreadSheetService.spreadSheet.serialize();
      if(this.isDateRange(cell) && !this.isValidDate(value)) {
        console.warn(`The date ${value} is invalid`);
        this.spreadSheetService.spreadSheet.setStyle(cell, { borderBottom: "1px solid #ff0000", color: "#ff0000" });
      } else {
        this.spreadSheetService.spreadSheet.setStyle(cell, {});
      }
      console.log('cell updated')
     // this.setData(saveData, 'spreadsheetData');

      this.cd.detectChanges();
    });
    //this.setupEvents();

    this.spreadSheetService.spreadSheet.events.on("afterSheetAdd", (sheet) => {
      console.log("A new sheet is added:", sheet.name);
      this.cloneSheetData();
      const saveData = this.spreadSheetService.spreadSheet.serialize();
      this.setData(saveData, 'spreadsheetData');
      console.log('prepare the data', saveData, this.spreadSheetService.spreadSheet.getValue('H2'))
    });
  }

  private isDateRange(cell: string): boolean {
    return cell.startsWith('A', 0);
  }

  private isValidDate(dateString: string): boolean {
    return dateString === '' || moment(dateString).isValid();
  }

  private setData(data: any, title: string): void { // TODO: create interface for json format
    const jsonData = JSON.stringify(data)
    localStorage.setItem(title, jsonData)
  }

  private getData(title: string): string | null {
    return localStorage.getItem(title)
  }

  public saveToNewSheet(event: Event): void {
    this.spreadSheetService.spreadSheet.addSheet(this._saveDataSheetName);
    this.cloneSheetData();
  }

  public saveToNewJson(event: Event): void {
    this.saveJsonData();
  }

  public addBigData(event: Event): void {
    this.createBigData().catch(console.log);
  }
  private saveJsonData(): void {

    const dataBaseJson: ICellInfo[] = [];

    this.spreadSheetService.spreadSheet.eachCell( (cell: string, value: any) => {
        dataBaseJson.push({cell, value});
    }, "H2:H1000");

    this.setData(dataBaseJson, 'saveToDB');
  }

  private cloneSheetData(): void {
    const { sheets, ...stuff } = this.spreadSheetService.spreadSheet.serialize();
    const [ arr0, arr1 ] = sheets;
    const { name, data } = arr0;
    const newSheetData = Array.from(data);
    const newData = {...stuff, 'sheets': [{...arr0}, { name: "saveToDatabase", data: [...newSheetData]}]};
    let tempObj: { cell: any, value: any }[] = [];
    //if(this.spreadSheetService.spreadSheet.getActiveSheet().name !== 'sheet1') {

   this.spreadSheetService.spreadSheet.selection.setSelectedCell("H2:H1000");
    this.spreadSheetService.spreadSheet.eachCell( (cell: string, value: any) => {
      if(cell && value) {
        console.log('eachCell', cell, value);
        tempObj.push({cell, value});
      }
    }, this.spreadSheetService.spreadSheet.selection.getSelectedCell());

    //JSON.parse(JSON.stringify(nestedObject));
    newSheetData.map((val: any, inx) => {
      if (val.cell === tempObj[inx]?.cell) {
        val.value = tempObj[inx]?.value;
        return val;
      }
      if (val.cell === tempObj[inx]?.cell) {
        val.value = tempObj[inx]?.value;
        return val;
      }
      return val;
    });
    //}
    console.log(stuff, tempObj, newSheetData,newData)
    this.spreadSheetService.spreadSheet.parse(newData)
  }

  private  async createBigData() {

    const updateCell = (cell : string, letter: string) => cell.replace('H', letter);

   const formulaValues = () => {
     return new Promise((resolve, reject) => {
       let totalsValue: string[] = [];
       try {
         this.spreadSheetService.spreadSheet.eachCell((cell: string, value: any) => {
           totalsValue.push(`=SUM(${updateCell(cell, 'B')},${updateCell(cell, 'D')})`);
         }, "H4:H3000");
         resolve(totalsValue);
         } catch (e) {
           reject(e);
         }
       })
     }

    await formulaValues().then(formulas => {
      this.spreadSheetService.spreadSheet.setValue('H4:H3000', formulas);
    }).catch(e => console.log('errors!'))
  }

  private setupEvents() {
    const events = [
      "beforeValueChange",
      "afterValueChange",
      "beforeStyleChange",
      "afterStyleChange",
      "beforeFormatChange",
      "afterFormatChange",
      "beforeSelectionSet",
      "afterSelectionSet",
      "beforeRowAdd",
      "afterRowAdd",
      "beforeRowDelete",
      "afterRowDelete",
      "beforeColumnAdd",
      "afterColumnAdd",
      "beforeColumnDelete",
      "afterColumnDelete",
      "beforeFocusSet",
      "afterFocusSet",
      "beforeEditStart",
      "afterEditStart",
      "beforeEditEnd",
      "afterEditEnd",
      "groupFill",
      "beforeSheetAdd",
      "afterSheetAdd",
      "beforeSheetRemove",
      "afterSheetRemove",
      "beforeSheetRename",
      "afterSheetRename",
      "beforeSheetChange",
      "afterSheetChange",
    ];
    events.forEach((event) => {
      this.spreadSheetService.spreadSheet.events.on(event, function () {
        console.log(event, arguments);
      });
    });
  }
}
